## LinguaMingle - Language Speaking Club Platform

LinguaMingle is a speaking club platform that facilitates connections among users for language learning and practice. The platform is built using Laravel, Inertia.js, and React.js, providing a modern and interactive experience for language enthusiasts.

## Features

User Authentication: 
- Users can register and log in to the platform using email and password credentials.

Language Selection: 
- Users can specify the languages they want to learn and the languages they can teach. (This feature is coming soon)

Speaking Club Events: 
- Users can create and join speaking club events to practice their desired languages.

Event Details: 
- Events include information about date, time, location, and featured languages.

Profile Management: 
- Users can update their profiles, upload profile pictures, and manage language preferences. (This feature is coming soon)

Dashboard: 
- A personalized dashboard displaying created events, participated events, and language learning preferences.

Admin Panel: 
- Admins have access to manage events, languages and user accounts.

Responsive Design: 
- The platform is designed to be responsive across various devices.

## Installation

Clone the repository: 
- git clone https://gitlab.com/rabbitishka/projet-final-lingua-mingle.git

Install PHP dependencies: 
- composer install

Install Node.js dependencies: 
- npm install

Create a .env file based on .env.example and configure your database settings.

Generate application key: 
- php artisan key:generate

Migrate the database: 
- php artisan migrate

Compile assets: 
- npm run dev

Start the development server: 
- php artisan serve

## Usage

Register an account or log in if you already have one.

Specify the languages you want to learn and teach in your profile. (This feature is coming soon)

Create speaking club events or join events organized by others.

Explore your personalized dashboard to manage your events and language preferences.

Admins can access the admin panel at /admin to manage events and user accounts.

## Technologies Used

Laravel: Backend framework for building robust and secure web applications.

Inertia.js: A modern approach to building single-page applications using server-side routing.

React.js: A JavaScript library for building user interfaces.

Tailwind CSS: A utility-first CSS framework for styling the user interface.

MySQL: Database management system for storing application data.

## Credits

LinguaMingle is developed by Aleksandra Rossi. It is an open-source project, and contributions are welcome.
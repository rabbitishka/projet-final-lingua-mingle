<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\EmailVerificationNoticeController;
use App\Http\Controllers\Auth\VerifyEmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/email/verify', [EmailVerificationPromptController::class, '__invoke'])
    ->middleware(['auth'])
    ->name('verification.notice');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/events/create', [EventController::class, 'create'])->name('events.create');
    Route::post('/events/{event}/participate', [EventController::class, 'participate'])->name('events.participate');
    Route::post('/events/{event}/cancel-participation', [EventController::class, 'cancelParticipation'])->name('events.cancel-participation');

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
});

Route::get('/contacts', function () {
    return Inertia::render('Contact/ContactCreate');
})->name('contacts');

Route::get('contacts/create', [ContactController::class, 'create'])->name('contacts.create');
Route::post('contacts/create', [ContactController::class, 'store'])->name('contacts.store'); 

Route::get('/events/show', function () {
    return Inertia::render('Event/EventDetails');
})->name('events/show');

Route::get('/events/{eventId}', [EventController::class, 'show'])->name('events.show');
Route::post('events/create', [EventController::class, 'store'])->name('events.store'); 
Route::get('/events', [EventController::class, 'index'])->name('events');

Route::get('/events/filter-by-language', [EventController::class, 'filterByLanguage'])->name('events.filterByLanguage');

Route::get('/cgu', function() {
    return Inertia::render('RGPD/CGU');
})->name('/cgu');

Route::get('/mentions-legales', function() {
    return Inertia::render('RGPD/MentionsLegales');
})->name('/mentions-legales');

Route::get('/politique-de-confidentialité', function() {
    return Inertia::render('RGPD/PolitiqueConfidentialité');
})->name('/mentions-legales');

require __DIR__.'/auth.php';

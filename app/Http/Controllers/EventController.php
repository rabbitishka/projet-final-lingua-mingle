<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Event;
use App\Models\Language;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $events = Event::all();
        $languages = Language::all();

        $languageId = $request->query('language_id');

        $eventsQuery = Event::query();

        if ($languageId) {
            $eventsQuery->where('language_id', $languageId);
        }

        $events = $eventsQuery->get();

        return Inertia::render('Event/EventsAll', [
            'auth' => Auth::user(),
            'events' => $events,
            'languages' => $languages,
    ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $languages = Language::all();

        // Redirecting to the login page if the user is not logged in
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        
        return Inertia::render('Event/EventCreate', [
            'languages' => $languages,
            'message' => session('message'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $eventData = $request->all();
        // If the user is authorized, get his id, otherwise leave null 
        $eventData['user_id'] = auth()->user() ? auth()->user()->id : null;
    

        Event::create($eventData);
    
        return redirect()->route('events.create')
                    ->with('message', 'Votre événement est créé avec succès ! Veuillez patienter pour la confirmation par administrateur');
    }

    public function filterByLanguage(Request $request)
    {
        $languageId = $request->input('language_id');
        
        $events = Event::where('language_id', $languageId)->get();
    
        return response()->json($events);
    }

    public function participate($eventId)
    {
        $event = Event::find($eventId);
        $user = auth()->user();

        if ($event) {
            $event->participants()->attach($user->id);
            session(['isGoing' => true]);
            $event->increment('participants_count');
            return response()->json(['message' => "Vous participez à l'événement !"]);
        } else {
            return response()->json(['message' => "L'événement n'existe pas."], 404);
        }
    }

    public function cancelParticipation($eventId)
    {
        $event = Event::findOrFail($eventId);
        $user = auth()->user();

        $event->participants()->detach($user->id);
        $event->decrement('participants_count');

        return response()->json(['message' => "Vous avez annulé votre participation à l'événement."]);
    }

    /**
     * Display the specified resource.
     */
    public function show($eventId)
    {
        $event = Event::with('participants', 'creator')->findOrFail($eventId);
        $languages = Language::all();

        if (!$event) {
            return Response::json(['error' => 'Event not found'], 404);
        }

        $user = auth()->user(); 

        // Redirecting to the login page if the user is not logged in
        if (!$user) {
            return redirect()->route('login')
                ->with('message', 'Veuillez vous connecter pour continuer');
        }

        return Inertia::render('Event/EventDetails', [
            'event' => $event,
            'languages' => $languages,
            'auth' => [
                'user' => [
                    'id' => $user->id,
                ],
            ],
            'isGoing' => $event->participants->contains($user->id),
            'participantsCount' => $event->participants->count(),
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Event $event)
    {
        //
    }
}

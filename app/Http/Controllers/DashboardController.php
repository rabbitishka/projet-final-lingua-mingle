<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Language;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $createdEvents = $user->createdEvents;
        $participatedEvents = $user->participatedEvents;
        $languages = Language::all();

        return Inertia::render('Dashboard', [
            'auth' => [
                'user' => [
                    'id' => $user->id,
                ],
            ],
            'createdEvents' => $createdEvents,
            'participatedEvents' => $participatedEvents,
            'languages' => $languages,
        ]);
    }
}

<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Contact;
use Illuminate\Support\Facades\Validator;
    
class ContactController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function create()
    {
        return Inertia::render('Contact/ContactCreate', [
            'message' => session('message'),
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => ['required'],
            'email' => ['required'],
            'message' => ['required'],
        ])->validate();
    
        Contact::create($request->all());
    
        return redirect()->route('contacts.create')
                    ->with('message', 'Votre message est envoyé avec succès !');
    }
}

<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    public function creator()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

    public function participants()
        {
            return $this->belongsToMany(User::class, 'event_user', 'event_id', 'user_id');
        }

    public function languageSpeaked()
        {
            return $this->belongsTo(Language::class, 'language_id');
        }
        
    use HasFactory;

    protected $fillable = ['title', 'description', 'theme', 'date_time', 'address', 'number_participants', 'participants_count', 'language_id', 'user_id'];
}

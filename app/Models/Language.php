<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Language extends Model
{
    use HasFactory;

    protected $fillable = ['language_name', 'flag_image'];

    public function allEvents() : HasMany
    {
        return $this->hasMany(Event::class);
    }
}
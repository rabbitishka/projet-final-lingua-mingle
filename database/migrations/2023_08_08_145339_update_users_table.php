<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // $table->renameColumn('id','id_users');
            // $table->renameColumn('name','first_name');
            // $table->string('last_name');
            // $table->string('username');
            // $table->string('city');
            // $table->string('mothertong');
            // $table->string('lang_of_interest');
            // $table->string('about_me');
            // $table->string('status');
            // $table->string('roles');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        Schema::dropIfExists('users');
    }
};

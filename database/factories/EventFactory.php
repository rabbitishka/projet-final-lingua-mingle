<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Event;
use App\Models\Language;
use Illuminate\Foundation\Auth\User;

class EventFactory extends Factory
{

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->paragraph(7),
            'theme' =>  $this->faker->word(),
            'date_time' => $this->faker->dateTime(),
            'address' => $this->faker->streetAddress(),
            'number_participants' => $this->faker->randomDigit(),
            'current_participants' => $this->faker->randomDigit(0-4),
            'language_id' => Language::inRandomOrder()->first()->id,
            'user_id' => User::inRandomOrder()->first()->id,
        ];
    }
}
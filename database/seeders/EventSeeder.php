<?php
namespace Database\Seeders;


use App\Models\Event;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $events = [
            [
                'title' => 'Cuisine Internationale',
                'description' => 'Venez découvrir et cuisiner des plats typiques de différents pays!',
                'theme' => 'Cuisine',
                'date_time' => now()->addDays(9)->format('Y-m-d H:i:s'),
                'address' => 'Le Café des Arts, 4 Rue Auguste Gaché, 38000 Grenoble',
                'number_participants' => 15,
                'participants_count' => 1,
                'language_id' => 9,
                'user_id' => 1,
            ],
            [
                'title' => 'Mode à Travers les Époques',
                'description' => 'Un voyage dans le temps à travers les styles vestimentaires du passé!',
                'theme' => 'Mode',
                'date_time' => now()->addDays(25)->format('Y-m-d H:i:s'),
                'address' => "L'Ampérage, 163 Cours Berriat, 38000 Grenoble",
                'number_participants' => 7,
                'participants_count' => 4,
                'language_id' => 3,
                'user_id' => 2,
            ],
            [
                'title' => 'Histoire et Culture',
                'description' => 'Explorons les événements historiques et les cultures du monde entier.',
                'theme' => 'Histoire',
                'date_time' => now()->addDays(3)->format('Y-m-d H:i:s'),
                'address' => 'Le Tram Cafe, Rue Gustave Flaubert, 38100 Grenoble',
                'number_participants' => 9,
                'participants_count' => 1,
                'language_id' => 8,
                'user_id' => 3,
            ],
            [
                'title' => 'Mentalités et Attitudes',
                'description' => 'Discutons des différences et des similarités dans les mentalités entre les pays.',
                'theme' => 'Société',
                'date_time' => now()->addDays(17)->format('Y-m-d H:i:s'),
                'address' => 'Bar Les Berthom, 12 Rue Bayard, 38000 Grenoble',
                'number_participants' => 7,
                'participants_count' => 6,
                'language_id' => 7,
                'user_id' => 4,
            ],
            [
                'title' => 'L\'Art de la Communication',
                'description' => 'Découvrez les différentes façons dont les langues influencent la communication.',
                'theme' => 'Langues',
                'date_time' => now()->addDays(6)->format('Y-m-d H:i:s'),
                'address' => '123 Rue de la Cuisine, Paris',
                'number_participants' => 10,
                'participants_count' => 8,
                'language_id' => 6,
                'user_id' => 5,
            ],
            [
                'title' => 'Cinéma du Monde',
                'description' => 'Explorons les films et les cinéastes de diverses cultures à travers le monde.',
                'theme' => 'Cinéma',
                'date_time' => now()->addDays(7)->format('Y-m-d H:i:s'),
                'address' => 'Le Café des Négociants, 2 Place de Lavalette, 38000 Grenoble',
                'number_participants' => 11,
                'participants_count' => 10,
                'language_id' => 5,
                'user_id' => 2,
            ],
            [
                'title' => 'Les Traditions Festives',
                'description' => 'Découvrez les célébrations et les traditions festives de différentes cultures.',
                'theme' => 'Traditions',
                'date_time' => now()->addDays(5)->format('Y-m-d H:i:s'),
                'address' => 'Brasserie Chavant, 3 Rue Gustave Eiffel, 38000 Grenoble',
                'number_participants' => 6,
                'participants_count' => 3,
                'language_id' => 4,
                'user_id' => 9,
            ],
            [
                'title' => 'L\'Évolution Technologique',
                'description' => 'Discutons de l\'impact de la technologie sur nos vies à l\'échelle mondiale.',
                'theme' => 'Technologie',
                'date_time' => now()->addDays(19)->format('Y-m-d H:i:s'),
                'address' => 'Le Stan, 3 Rue Condorcet, 38000 Grenoble',
                'number_participants' => 9,
                'participants_count' => 8,
                'language_id' => 3,
                'user_id' => 7,
            ],
            [
                'title' => 'Exploration Musicale',
                'description' => 'Écoutons et partageons des musiques provenant de divers coins du monde.',
                'theme' => 'Musique',
                'date_time' => now()->addDays(33)->format('Y-m-d H:i:s'),
                'address' => 'Le Drak-Art, 163 Cours Berriat, 38000 Grenoble',
                'number_participants' => 13,
                'participants_count' => 6,
                'language_id' => 2,
                'user_id' => 6,
            ],
            [
                'title' => 'Diversité Linguistique',
                'description' => 'Explorons la richesse et la diversité des langues parlées à travers le monde.',
                'theme' => 'Langues',
                'date_time' => now()->addDays(23)->format('Y-m-d H:i:s'),
                'address' => 'Le Barberousse, 13 Rue des Clercs, 38000 Grenoble',
                'number_participants' => 14,
                'participants_count' => 5,
                'language_id' => 1,
                'user_id' => 7,
            ],
        ];

        foreach ($events as $eventData) {
            Event::create($eventData);
        }

        // Event::factory(3)->create();
    }
}
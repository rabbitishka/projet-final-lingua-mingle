<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $languages = [
            [
                'language_name' => 'Anglais',
                'flag_image' => '/imgs/flags/great_britain.svg',
            ],
            [
                'language_name' => 'Français',
                'flag_image' => '/imgs/flags/france.svg',
            ],
            [
                'language_name' => 'Chinois',
                'flag_image' => '/imgs/flags/china.svg',
            ],
            [
                'language_name' => 'Suédois',
                'flag_image' => '/imgs/flags/sweden.svg',
            ],
            [
                'language_name' => 'Russe',
                'flag_image' => '/imgs/flags/russia.svg',
            ],
            [
                'language_name' => 'Espagnol',
                'flag_image' => '/imgs/flags/spain.svg',
            ],
            [
                'language_name' => 'Italien',
                'flag_image' => '/imgs/flags/italy.svg',
            ],
            [
                'language_name' => 'Allemand',
                'flag_image' => '/imgs/flags/germany.svg',
            ],
            [
                'language_name' => 'Coréen',
                'flag_image' => '/imgs/flags/korea.svg',
            ],
        ];

        foreach ($languages as $languageData) {
            Language::create($languageData);
        }
    }

}
<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        User::create([
            'name' => 'alex',
            'email' => 'alex@alex.fr',
            'password' => Hash::make('alexalex'),
            'email_verified_at' => now(),
        ]);

        User::create([
            'name' => 'Aleksandra R',
            'email' => 'aleksandra.rossi.simplon@gmail.com',
            'password' => Hash::make('komnata_123'),
            'email_verified_at' => now(),
        ]);

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);

        User::factory(10)->create();
    }
}

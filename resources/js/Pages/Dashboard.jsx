import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';
import { Head, usePage } from '@inertiajs/react';
import React from 'react';
import EventCard from '@/Pages/Event/EventCard';

export default function Dashboard({ }) {

    const { auth, createdEvents, participatedEvents, languages  } = usePage().props;

    return (
        <>
            <Head title="Dashboard" />
            <Header auth={auth}></Header>
            <div className="min-h-[calc(100vh-16rem)] p-8">
                <h2 className='md:text-start text-center text-[24px] py-8 md:pl-9 '>Evénements que j'ai créés : </h2>
                {createdEvents && createdEvents.length > 0 ? (
                    <ul className='flex flex-col items-center md:flex-row md:flex-wrap md:justify-center gap-6 mx-auto'>
                        {createdEvents && createdEvents.map(event => (
                            <EventCard key={event.id} event={event} languages={languages} />
                        ))}
                    </ul>
                ) : (
                    <p className="text-center text-gray-600">Vous n'avez pas encore créé d'événement.</p>
                )}
                <h2 className='md:text-start text-center text-[24px] py-8 md:pl-9'>Evénements auxquels je participe : </h2>
                {participatedEvents && participatedEvents.length > 0 ? (
                    <ul className='flex flex-col items-center md:flex-row md:flex-wrap md:justify-center gap-6 mx-auto'>
                        {participatedEvents && participatedEvents.map(event => (
                            <EventCard key={event.id} event={event} languages={languages} />
                        ))}
                    </ul>
                ) : (
                    <p className="text-center text-gray-600">Vous ne participez actuellement à aucun événement.</p>
                )}
            </div>
            <Footer></Footer> 
        </>
    );
}

import { Head } from '@inertiajs/react';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';



export default function PolitiqueConfidentialité({ auth }) {
    return (
        <>
        <div className="max-w-7xl mx-auto p-4">
            <Head title="Politique de Confidentialité"/>
            <Header auth={auth}></Header>
            <main>
                <div className='flex flex-col items-center gap-4 p-10'>
                    <h2 className='font-semibold'>Politique de Confidentialité</h2>
                    
                    <p>La présente Politique de Confidentialité régit la manière dont Aleksandra Rossi (désigné ci-après comme "nous", "notre" ou "l'éditeur") collecte, utilise, divulgue et protège les informations personnelles des utilisateurs ("vous" ou "utilisateurs") de la plateforme en ligne "LinguaMingle". Veuillez prendre le temps de lire attentivement cette politique pour comprendre comment nous traitons vos données personnelles.</p>

                            <p>1. Collecte des Informations : </p>

                            <p>Nous pouvons collecter des informations personnelles lorsque vous vous inscrivez sur notre plateforme, utilisez nos services, communiquez avec nous ou participez à nos activités. Les informations collectées peuvent inclure votre nom, votre adresse e-mail, votre âge, votre sexe, vos préférences linguistiques et d'autres données pertinentes.</p>

                            <p>2. Utilisation des Informations :</p>

                            <p>Nous utilisons vos informations personnelles pour améliorer votre expérience sur la plateforme LinguaMingle, personnaliser nos services en fonction de vos préférences, faciliter les interactions entre les membres de nos clubs de conversation et vous fournir des informations pertinentes. Vos données peuvent également être utilisées à des fins de communication et de marketing, avec votre consentement.</p>

                            <p>3. Partage des Informations :</p>

                            <p>Vos informations personnelles ne seront pas vendues, louées ou échangées à des tiers à des fins commerciales. Cependant, dans le cadre de nos activités, nous pouvons partager certaines informations avec nos partenaires, prestataires de services ou autorités compétentes, lorsque requis par la loi.</p>

                            <p>4. Sécurité des Données :</p>

                            <p>Nous prenons des mesures techniques et organisationnelles pour protéger vos données personnelles contre l'accès non autorisé, la divulgation, la perte ou la destruction. Cependant, aucune méthode de transmission sur Internet ni méthode de stockage électronique n'est totalement sécurisée. Par conséquent, bien que nous nous efforcions de protéger vos informations, nous ne pouvons garantir leur sécurité absolue.</p>

                            <p>5. Vos Choix :</p>

                            <p>Vous avez le droit de consulter, corriger, mettre à jour ou supprimer vos informations personnelles. Vous pouvez également choisir de ne pas recevoir nos communications marketing. Pour exercer ces droits, veuillez nous contacter à l'adresse aleksandra.rossi.simplon@gmail.com.</p>

                            <p>6. Cookies :</p>

                            <p>Nous utilisons des cookies et des technologies similaires pour collecter des informations sur votre utilisation de la plateforme LinguaMingle. Vous pouvez gérer vos préférences en matière de cookies via les paramètres de votre navigateur.</p>

                            <p>7. Modifications de la Politique :</p>

                            <p>Nous nous réservons le droit de modifier cette Politique de Confidentialité à tout moment. Les modifications seront effectives dès leur publication sur notre site web. Nous vous encourageons à consulter régulièrement cette politique pour rester informé des éventuelles mises à jour.</p>

                            <p>8. Contact :</p>

                            <p>Pour toute question ou demande concernant notre Politique de Confidentialité, veuillez nous contacter à l'adresse aleksandra.rossi.simplon@gmail.com.</p>

                            <p>Fait à Grenoble, le 30.08.2023.</p>

                            <p></p>
                    
                </div>
            </main>
            <Footer></Footer>
        </div>
        </>
    );
}
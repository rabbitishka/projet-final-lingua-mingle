import { Head } from '@inertiajs/react';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';



export default function MentionsLegales({ auth }) {
    return (
        <>
        <div className="max-w-7xl mx-auto p-4">
            <Head title="Mentions Légales"/>
            <Header auth={auth}></Header>
            <main>
                <div className='flex flex-col items-center gap-4 p-10'>
                    <h2 className='font-semibold'>Mentions Légales</h2>
        
                            <p>1. Informations légales : </p>

                            <p>Le site web et la plateforme en ligne "LinguaMingle" sont édités et gérés par Aleksandra Rossi (désigné ci-après comme "nous", "notre" ou "l'éditeur"). Siège social : 34 Av. de l'Europe Bâtiment D, 38100 Grenoble. Numéro de téléphone : 06 22 17 97 17. Adresse e-mail : aleksandra.rossi.simplon@gmail.com.</p>

                            <p>2. Hébergement :</p>

                            <p>Le site web et la plateforme LinguaMingle sont hébergés par YunoHost.</p>

                            <p>3. Propriété intellectuelle :</p>

                            <p>Tous les éléments présents sur le site web LinguaMingle, y compris les textes, les images, les vidéos, les logos et les icônes, sont la propriété exclusive de l'éditeur ou font l'objet d'une autorisation d'utilisation. Toute reproduction, représentation, modification, distribution ou exploitation, même partielle, de ces éléments sans l'autorisation préalable écrite de l'éditeur est strictement interdite et pourra donner lieu à des poursuites judiciaires.</p>

                            <p>4. Utilisation d'images vectorielles :</p>

                            <p>Certains éléments visuels utilisés sur notre site web LinguaMingle sont issus du site undraw.co et sont soumis à la licence Creative Commons Attribution 3.0. Les crédits correspondants aux illustrations utilisées sont visibles sur notre site, conformément aux exigences de la licence. Nous remercions undraw.co pour leur contribution visuelle à notre plateforme.</p>

                            <p>5. Utilisation d'images aléatoires :</p>

                            <p>Nous utilisons des images aléatoires provenant de l'API Unsplash pour enrichir notre site web LinguaMingle. Unsplash est une communauté d'artistes qui partagent des photos de haute qualité sous licence libre. Les images sont utilisées conformément à leurs termes de licence.</p>

                            <p>Les crédits des images utilisées peuvent être trouvés sur les pages respectives où ces images sont affichées. Nous remercions la communauté Unsplash pour ses contributions visuelles à notre plateforme.</p>
                            
                            <p>6. Collecte et traitement des données personnelles :</p>

                            <p>Nous collectons et traitons les données personnelles conformément à notre politique de confidentialité. Vous pouvez consulter notre politique pour en savoir plus sur les données que nous collectons, comment nous les utilisons et comment vous pouvez exercer vos droits en matière de protection des données.</p>

                            <p>7. Responsabilité :</p>

                            <p>L'éditeur met en œuvre tous les moyens nécessaires pour assurer la qualité et la sécurité de la plateforme LinguaMingle. Cependant, l'éditeur ne saurait être tenu responsable des éventuels dysfonctionnements, interruptions ou virus affectant le fonctionnement de la plateforme.</p>

                            <p>8. Contact :</p>

                            <p>Pour toute question ou demande concernant les mentions légales, veuillez nous contacter à l'adresse suivante : aleksandra.rossi.simplon@gmail.com.</p>

                            <p>9. Loi applicable :</p>

                            <p>Les présentes mentions légales sont régies par le droit français. En cas de litige, les tribunaux français seront seuls compétents.</p>

                            <p>Fait à Grenoble, le 30.08.2023.</p>

                            <p></p>
                    
                </div>
            </main>
            <Footer></Footer>
        </div>
        </>
    );
}
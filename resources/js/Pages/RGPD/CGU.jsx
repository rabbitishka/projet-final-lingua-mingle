import { Head } from '@inertiajs/react';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';



export default function CGU({ auth }) {
    return (
        <>
        <div className="max-w-7xl mx-auto p-4">
            <Head title="CGU"/>
            <Header auth={auth}></Header>
            <main>
                <div className='flex flex-col items-center gap-4 p-10'>
                    <h2 className='font-semibold'>Conditions Générales d'Utilisation (CGU)</h2>
                    
                    <p>Les présentes Conditions Générales d'Utilisation (désignées ci-après comme "CGU") régissent l'utilisation de la plateforme en ligne "LinguaMingle" éditée et gérée par Aleksandra Rossi (désigné ci-après comme "nous", "notre" ou "l'éditeur"). En utilisant notre plateforme, vous acceptez ces CGU dans leur intégralité. Si vous n'acceptez pas ces conditions, veuillez ne pas utiliser notre service.</p>

                            <p>1. Utilisation de la Plateforme : </p>

                            <p>L'utilisation de la plateforme LinguqMingle est réservée aux personnes qui ont l'âge légal de consentement dans leur juridiction. Vous acceptez de fournir des informations exactes, à jour et complètes lors de votre inscription et de maintenir ces informations à jour.</p>

                            <p>2. Comptes Utilisateur :</p>

                            <p>Lors de l'inscription, vous choisirez un identifiant et un mot de passe. Vous êtes responsable de la confidentialité de vos informations d'identification et de toute activité qui se produit sur votre compte. En cas d'utilisation non autorisée de votre compte, veuillez nous en informer immédiatement.</p>

                            <p>3. Contenu Utilisateur :</p>

                            <p>En utilisant la plateforme, vous pouvez soumettre du contenu tel que des messages, des commentaires ou des contributions. Vous conservez les droits de propriété intellectuelle sur ce contenu. En nous fournissant du contenu, vous nous accordez une licence non exclusive, mondiale et gratuite pour utiliser, afficher, modifier et distribuer ce contenu dans le cadre de la prestation de nos services.</p>

                            <p>4. Conduite de l'Utilisateur :</p>

                            <p>Vous vous engagez à utiliser la plateforme conformément aux lois en vigueur et aux normes éthiques. Vous ne devez pas publier de contenu offensant, diffamatoire, illégal ou contraire à nos valeurs. Nous nous réservons le droit de supprimer tout contenu qui enfreint ces directives.</p>

                            <p>5. Propriété Intellectuelle :</p>

                            <p>Tous les éléments de la plateforme LinguaMingle, y compris le texte, les images, les logos et les marques, sont la propriété exclusive de l'éditeur ou font l'objet d'une autorisation d'utilisation. Toute reproduction, distribution ou utilisation non autorisée de ces éléments est strictement interdite.</p>

                            <p>6. Responsabilité :</p>

                            <p>Nous nous efforçons de maintenir la plateforme opérationnelle et sécurisée, mais nous ne garantissons pas son fonctionnement ininterrompu et exempt d'erreurs. Nous ne sommes pas responsables des dommages directs, indirects, spéciaux ou consécutifs résultant de l'utilisation de notre plateforme.</p>

                            <p> 7. Modifications des CGU :</p>

                            <p>Nous nous réservons le droit de modifier ces CGU à tout moment. Les modifications seront effectives dès leur publication sur notre site web. En continuant à utiliser la plateforme après de telles modifications, vous acceptez les nouvelles CGU.</p>

                            <p>8. Loi Applicable :</p>

                            <p>Les présentes CGU sont régies par le droit français. En cas de litige, les tribunaux français seront seuls compétents.</p>

                            <p>Fait à Grenoble, le 30.08.2023.</p>

                            <p></p>
                    
                </div>
            </main>
            <Footer></Footer>
        </div>
        </>
    );
}
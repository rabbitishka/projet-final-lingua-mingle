import { Link, Head } from '@inertiajs/react';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';
import Banner from '@/Components/Banner';
import SectionWhyUs from '@/Components/SectionWhyUs';
import SectionEvents from '@/Components/SectionEvents';

export default function Welcome({ auth }) {
    return (
        <>
        <div className="max-w-7xl mx-auto p-4">
            <Head title="Bienvenue"/>
            <Header auth={auth}></Header>
            <Banner></Banner> 
            <SectionWhyUs></SectionWhyUs>
            <SectionEvents></SectionEvents>
            <Footer></Footer>
        </div>
        </>
    );
}

import { useEffect } from 'react';
import Checkbox from '@/Components/Checkbox';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';
import AuthenticationImage from '../../../imgs/authentication.svg';

export default function Login({ status, canResetPassword }) {
    
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        password: '',
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <GuestLayout>
            <Head title="Connexion" />

            {status && <div className="mb-4 font-medium text-sm text-green-600">{status}</div>}

            <div className=' '>
                <img src={AuthenticationImage} alt="Authentication picture" loading="lazy"/>
            </div>

            <h2 className='text-center text-[32px] py-5'>Connexion</h2>

            <form onSubmit={submit}>
                <div>
                    <InputLabel htmlFor="email" value="Email" />

                    <TextInput
                        id="email"
                        type="email"
                        name="email"
                        value={data.email}
                        className="mt-1 block w-full border rounded-full"
                        autoComplete="email"
                        isFocused={true}
                        onChange={(e) => setData('email', e.target.value)}
                    />

                    <InputError message={errors.email} className="mt-2" />
                </div>

                <div className="mt-4 border border-transparent rounded-full">
                    <InputLabel htmlFor="password" value="Mot de passe" />

                    <TextInput
                        id="password"
                        type="password"
                        name="password"
                        value={data.password}
                        className="mt-1 block w-full"
                        autoComplete="current-password"
                        onChange={(e) => setData('password', e.target.value)}
                    />

                    <InputError message={errors.password} className="mt-2" />
                </div>

                <div className="flex flex-col items-center mt-4">
                    <div className="flex items-center mt-4">
                        <label className="">
                            <Checkbox
                                name="remember"
                                checked={data.remember}
                                onChange={(e) => setData('remember', e.target.checked)}
                            />
                            <span className="ml-2 text-sm">Me souvenir</span>
                        </label>
                </div>
                
                <div className='pb-2 pt-2'>
                        <PrimaryButton className="ml-4" disabled={processing}>
                            Se connecter
                        </PrimaryButton>
                </div>
                    
                <div>
                    {canResetPassword && (
                        <Link
                            href={route('password.request')}
                            className="underline text-sm rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800"
                        >
                            Mot de passe oublié ?
                        </Link>
                        
                    )}
                </div>

                <div className='py-9'>
                        <span className='text-sm'>Pas de compte ?</span>
                        <Link
                            href={route('register')}
                            className="underline text-sm rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800"
                        >
                            S'inscrire
                        </Link>
                        </div>
                </div>
            </form>
        </GuestLayout>
    );
}

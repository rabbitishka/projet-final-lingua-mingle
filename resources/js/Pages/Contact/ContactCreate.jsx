import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, useForm, usePage } from '@inertiajs/react';
import ContactUsImage from '../../../imgs/contact_us.svg';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';

export default function ContactCreate({ auth }) {
    
    const { message } = usePage().props

    const { data, setData, post, errors, processing, reset } = useForm({
        name: '',
        email: '',
        message: '',
    });

    const submit = (e) => {
        e.preventDefault();
        reset();

        post(route('contacts.store'));
    };

    
    return (
        <>
            <Head title="Page de contact"/>
            <Header auth={auth}></Header>
            <div className='min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-white dark:bg-gray-900'>
                <div className="w-full sm:max-w-md mt-6 px-6 py-4 bg-white dark:bg-gray-800 overflow-hidden sm:rounded-lg">
                    <img src={ContactUsImage} alt="Contuct us picture" loading="lazy"/>
                    <h1 className='text-center text-[32px] py-5'>Contactez-nous</h1>

                    { message &&
                        <div className="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
                            <div className="flex">
                                <div>
                                    <p className="text-sm">{message}</p>
                                </div>
                            </div>
                        </div>
                    }

                    <form onSubmit={submit}>
                        <div className="mt-4">
                            <InputLabel htmlFor="name" value="Prénom/Nom d'utilisateur" />

                            <TextInput
                                id="name"
                                name="name"
                                value={data.name}
                                className="mt-1 block w-full"
                                autoComplete="name"
                                isFocused={true}
                                onChange={(e) => setData('name', e.target.value)}
                                required
                            />

                            <InputError message={errors.name} className="mt-2" />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="email" value="Email" />

                            <TextInput
                                id="email"
                                type="email"
                                name="email"
                                value={data.email}
                                className="mt-1 block w-full"
                                autoComplete="username"
                                onChange={(e) => setData('email', e.target.value)}
                                required
                            />

                            <InputError message={errors.email} className="mt-2" />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="message" value="Message" />

                            <textarea
                                id="message"
                                type="text"
                                name="message"
                                value={data.message}
                                className="mt-1 block w-full h-60 border-gray-300 dark:border-gray-700 dark:bg-gray-900 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-lg shadow-sm"
                                onChange={(e) => setData('message', e.target.value)}
                                required
                            />

                            <InputError message={errors.message} className="mt-2" />
                        </div>

                        <div className='flex flex-col items-center justify-center mt-4'>
                            <PrimaryButton className="ml-4" disabled={processing}>
                                Envoyer
                            </PrimaryButton>
                        </div>
                    </form>
            </div>
            </div>
            <Footer></Footer>
        </>
    );
}
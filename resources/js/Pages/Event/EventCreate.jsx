import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, useForm, usePage } from '@inertiajs/react';
import FolderImage from '../../../imgs/image_folder.svg';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';
import Select from 'react-select';

export default function EventCreate({ auth, languages }) {

    const { message } = usePage().props

    const { data, setData, post, errors, processing, reset } = useForm({
        title: '',
        description: '',
        theme: '',
        date_time: '',
        address: '',
        number_participants: '',
        language_id: '',
    });

    const submit = (e) => {
        e.preventDefault();
        reset();

        post(route('events.store'));
    };

    const languageOptions = languages.map(language => ({
        value: language.id,
        label: (
            <div className="flex items-center">
                <img src={language.flag_image} alt={language.language_name} className="w-6 h-6 inline-block mr-2" />
                {language.language_name}
            </div>
        ),
    }));


    return (
        <>
            <Head title="Proposition d'événement"/>
            <Header auth={auth}></Header>
            <div className='flex flex-col md:flex-row justify-center p-4 gap-20 md:gap-48'>
                <div className='md:w-68 md:h-60 pt-20'><img src={FolderImage} alt="Image Folder picture" loading="lazy"/></div>
                <div className="flex flex-col justify-center">
                    <h1 className='text-center text-[32px] py-5'>Proposez votre événement</h1>

                    { message &&
                        <div className="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
                            <div className="flex">
                                <div>
                                    <p className="text-sm">{message}</p>
                                </div>
                            </div>
                        </div>
                    }

                    <form onSubmit={submit}>

                    <div className="mt-4">
                            <InputLabel htmlFor="title" value="Le titre" />

                            <TextInput
                                id="title"
                                type="text"
                                name="title"
                                value={data.title}
                                className="mt-1 block w-full"
                                onChange={(e) => setData('title', e.target.value)}
                                required
                            />

                            <InputError message={errors.title} className="mt-2" />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="description" value="Description" />

                            <textarea
                                id="description"
                                type="text"
                                name="description"
                                value={data.description}
                                className="mt-1 block w-full h-60 border-gray-300 dark:border-gray-700 dark:bg-gray-900 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-lg shadow-sm"
                                onChange={(e) => setData('description', e.target.value)}
                                required
                            />

                            <InputError message={errors.description} className="mt-2" />
                        </div>
                        <div>
                            <InputLabel htmlFor="language_id">Vouz allez parler quelle langue ?</InputLabel>

                            <Select
                                id="language_id"
                                name='language_choice'
                                options={languageOptions}
                                value={languageOptions.find(option => option.value === data.language_id)}
                                onChange={selectedOption => setData('language_id', selectedOption.value)}
                                placeholder='Selectionner'
                                required
                            />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="theme" value="Quelle est le thème ?" />

                            <TextInput
                                id="theme"
                                type="text"
                                name="theme"
                                value={data.theme}
                                className="mt-1 block w-full"
                                onChange={(e) => setData('theme', e.target.value)}
                                required
                            />

                            <InputError message={errors.theme} className="mt-2" />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="date_time" value="Quand est l'événement ?" />

                            <TextInput
                                id="date_time"
                                type="datetime-local"
                                name="date_time"
                                value={data.date_time}
                                className="mt-1 block w-full"
                                onChange={(e) => setData('date_time', e.target.value)}
                                required
                            />

                            <InputError message={errors.date_time} className="mt-2" />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="title" value="Quelle est l'adresse ?" />

                            <TextInput
                                id="address"
                                type="text"
                                name="address"
                                autoComplete="address"
                                value={data.address}
                                className="mt-1 block w-full"
                                onChange={(e) => setData('address', e.target.value)}
                                required
                            />

                            <InputError message={errors.address} className="mt-2" />
                        </div>

                        <div className="mt-4">
                            <InputLabel htmlFor="number" value="Le maximum de participants ?" />

                            <TextInput
                                id="number"
                                type="number" min="0"
                                name="number"
                                value={data.number_participants}
                                className="mt-1 block w-full"
                                onChange={(e) => setData('number_participants', e.target.value)}
                                required
                            />

                            <InputError message={errors.number_participants} className="mt-2" />
                        </div>

                        <div className='flex flex-col items-center justify-center mt-4'>
                            <PrimaryButton className="ml-4" disabled={processing}>
                                Créer
                            </PrimaryButton>
                        </div>
                    </form>
                </div>
            </div>
            <Footer></Footer>
        </>
    );
}
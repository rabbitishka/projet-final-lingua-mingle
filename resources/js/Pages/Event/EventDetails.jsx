import { Head, usePage  } from '@inertiajs/react';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';
import FolderImage from '../../../imgs/image_folder.svg';
import PrimaryButton from '@/Components/PrimaryButton';
import SecondaryButton from '@/Components/SecondaryButton';
import React, { useState } from 'react';
import { format } from 'date-fns';
import axios from 'axios';
import useRandomUnsplashImage from '@/hooks/useRandomUnsplashImage';

export default function EventDetails({ event, auth, languages, isGoing, participantsCount, setParticipantsCount }) {

    const [participationMessage, setParticipationMessage] = useState('');

    const [localIsGoing, setLocalIsGoing] = useState(isGoing);

    const handleGoingClick = async () => {
        try {
          await axios.post(route('events.participate', { event: event.id }));
        // Request to server for updated data
          const response = await axios.get(`/events/${event.id}`);
          const updatedEvent = response.data.event;

          setLocalIsGoing(true);
          setParticipationMessage("Vous participez à l'événement !");

          setParticipantsCount(updatedEvent.participants_count);
        } catch (error) {
          console.error('Error when participating in an event:', error);
        }
    };
    
    const handleCancelClick = async () => {
        try {
          await axios.post(route('events.cancel-participation', { event: event.id }));
          // Request to server for updated data
          const response = await axios.get(`/events/${event.id}`);
          const updatedEvent = response.data.event;

          setLocalIsGoing(false);
          setParticipationMessage("Vous avez annulé votre participation à l'événement.");

          setParticipantsCount(updatedEvent.participants_count);
        } catch (error) {
          console.error('Error when canceling membership:', error);
        }
    };

    const remainingSpots = Math.max(0, event.number_participants - participantsCount);

    const selectedLanguage = languages?.find(language => language.id === event?.language_id) || null;

    const { randomImage, loading } = useRandomUnsplashImage();

    return (
        <>
        <Head title='Evénement'/>
        <Header auth={auth}></Header>
            <div className='flex flex-col md:flex-row justify-center p-4 gap-20 md:gap-48 overflow-hidden'>
                {/* <div className='md:w-68 md:h-60 pt-20 md:basis-1/4'><img src={FolderImage} alt="Image Folder picture" loading="lazy"/></div> */}
                {/* {!loading && randomImage.authorInfo ? ( */}
                {!loading ? (
                <>
                <div className='md:w-68 md:h-60 pt-20 md:basis-1/4'>
                    {randomImage && (
                        <img src={randomImage} alt="Random Unsplash Image" />
                    )}
                        {/* <img src={randomImage.imageUrl} alt="Random Unsplash Image" />
                        <p>Auteur: {randomImage.authorInfo.name}</p> */}
                </div>
                </>
                ) : (
                    <div>Chargement...</div>
                  )}
                <div className="flex flex-col justify-center md:basis-1/2 gap-8 md:gap-16">
                    <h1 className='text-center text-[32px] py-5'>{event?.title}</h1>
                    <p>{event?.description}</p>
                    <p className='md:place-self-end'> Langue :  
                        {selectedLanguage ? (
                            <span>
                                <img src={selectedLanguage.flag_image} alt={selectedLanguage.language_name} className='w-6 h-6 inline-block mx-2' />
                                {selectedLanguage.language_name}
                            </span>
                        ) : 'Langue inconnue'}
                    </p>
                    <p className='md:place-self-end'>Quand : {event ? format(new Date(event.date_time), 'dd.MM.yyyy HH:mm'): ''}</p>
                    <p className='md:place-self-end'>Le thème: {event?.theme}</p>
                    <p className='md:place-self-end'>L'addresse : {event?.address}</p>
                    <p className='md:place-self-end'>Max : {event?.number_participants} personnes</p>
                    <p className='md:place-self-end'>Places restantes : {remainingSpots}</p>
                </div>
            </div>
            <div className='flex justify-center md:justify-end py-9 md:pr-12'>
            {localIsGoing  ? (
                <>
                    <SecondaryButton onClick={handleCancelClick}>J'annule</SecondaryButton>
                </>
            ) : (
                <>
                    <PrimaryButton onClick={handleGoingClick}>J'y vais !</PrimaryButton>
                </>
            )}
            </div>
            {participationMessage && (
                <div className="text-center text-green-500 mb-4 flex justify-center md:justify-end md:pr-12">{participationMessage}</div>
            )}
        <Footer></Footer>
        </>
    );
}
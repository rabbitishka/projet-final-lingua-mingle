import { Link, Head } from '@inertiajs/react';
import Header from '@/Layouts/Header';
import Footer from '@/Layouts/Footer';
import EventCard from '@/Pages/Event/EventCard';
import ConnectedWorldImage from '../../../imgs/connected_world.svg';
import SecondaryButton from '@/Components/SecondaryButton';
import React, { useState } from 'react';


export default function EventsAll({ auth, events, languages }) {

    const [isLanguagesOpen, setIsLanguagesOpen] = useState(false);

    const [selectedLanguage, setSelectedLanguage] = useState(null);
    const [filteredEvents, setFilteredEvents] = useState(events);

    function handleLanguageChange(languageId) {
        setSelectedLanguage(languageId);
    
        if (languageId === null) {
            setFilteredEvents(events);
          } else {
            const filtered = events.filter(event => event.language_id === languageId);
            setFilteredEvents(filtered);
          }
    };

    return (
        <>
            <Head title="Événements"/>
            <Header auth={auth}></Header>
                <div className='flex flex-col justify-center items-center'>
                    <h1 className='text-center text-[32px] py-5'>Événements</h1>
                    <div className='flex md:flex-row flex-col justify-evenly'>
                        <div className='place-self-center py-9 md:pr-12'>
                            <SecondaryButton>
                                <Link href={route('events.create')}>
                                    Je propose
                                </Link>
                            </SecondaryButton>
                        </div>
                        <div className='md:place-self-center md:basis-1/4'>
                            <img src={ConnectedWorldImage} alt="Connected world picture" loading="lazy"/>
                        </div>
                    </div>
                </div>

                <div className='flex md:flex-row flex-col justify-center items-center md:justify-between mx-auto mt-8'>

                    <div className="mt-4 hidden md:block md:pr-8 md:pl-6 mb-4 md:mb-0">
                        <ul>
                            <div onClick={() => handleLanguageChange(null)} className="flex items-center gap-2 p-4 border border-black rounded-full cursor-pointer mb-3 transition-colors hover:bg-indigo-300 hover:text-white active:bg-indigo-500">
                                <span>Sans filtre</span>
                            </div>
                            {languages.map(language => (
                                <li
                                    key={language.id}
                                    className={`flex items-center gap-2 p-4 pr-8 border rounded-full mb-3 cursor-pointer transition-colors hover:bg-indigo-300 hover:text-white ${
                                    selectedLanguage === language.id ? 'bg-indigo-300 text-white' : 'border-black'
                                    }`}
                                    onClick={() => handleLanguageChange(language.id)}
                                >
                                    <img src={language.flag_image} alt={language.language_name} className="w-6 h-6" />
                                    <span>{language.language_name}</span>
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className='md:hidden text-center m-8'>
                        <button
                            className='p-4 border rounded-full border-black transition-colors hover:bg-indigo-300 hover:text-white active:bg-indigo-500'
                            onClick={() => setIsLanguagesOpen(!isLanguagesOpen)}
                        >
                            {isLanguagesOpen ? 'Masquer les langues' : 'Afficher les langues'}
                        </button>

                        {isLanguagesOpen && (
                            <ul className='mt-2'>
                            <li
                                className='mb-2 cursor-pointer transition-colors hover:bg-indigo-300 hover:text-white'
                                onClick={() => handleLanguageChange(null)} 
                            >
                                <span>Sans filtre</span>
                            </li>
                            {languages.map(language => (
                                <li
                                key={language.id}
                                className='mb-2 cursor-pointer transition-colors hover:bg-indigo-300 hover:text-white'
                                onClick={() => handleLanguageChange(language.id)}
                                >
                                <img src={language.flag_image} alt={language.language_name} className='w-6 h-6 inline-block mr-2' />
                                <span>{language.language_name}</span>
                                </li>
                            ))}
                            </ul>
                        )}
                    </div>

                    <div className='grid grid-cols-1 md:grid-cols-3 gap-6 mx-auto mt-8'>
                        {filteredEvents.map(event => (
                            <div key={event.id} className='col-span-1 md:col-span-1'>
                                <EventCard 
                                    event={event} 
                                    languages={languages}
                                />
                            </div>
                        ))}
                    </div>

                </div>

                <div className='flex justify-center md:justify-end py-9 md:pr-12'>
                    <SecondaryButton>
                        <Link href={route('events.create')}>Je propose</Link>
                    </SecondaryButton>
                </div>
            <Footer></Footer>   
                  
        </>
    );
}

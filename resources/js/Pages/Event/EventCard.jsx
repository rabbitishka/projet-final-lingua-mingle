import { Link } from '@inertiajs/react';
import PrimaryButton from '@/Components/PrimaryButton';
import { format } from 'date-fns';

export default function EventCard({ event, languages }) {

    const selectedLanguage = languages.find(language => language.id === event.language_id);

    return (
        <div className="flex flex-row space-x-4">
            <div className="border rounded-lg p-4 shadow-md">
                <div className="flex items-center">
                    {selectedLanguage && (
                        <img src={selectedLanguage.flag_image} alt={selectedLanguage.language_name} className="w-6 h-6" />
                    )}
                    <p className='md:place-self-end'>
                       {selectedLanguage ? selectedLanguage.language_name : 'Langue inconnue'}
                    </p>
                </div>
                <p className='md:place-self-end'>Quand : {format(new Date(event.date_time), 'dd.MM.yyyy HH:mm')}</p>
                <p className='md:place-self-end'>Le thème: {event.theme}</p>
                <div className='flex justify-center py-4'>
                    <PrimaryButton><Link href={`/events/${event.id}`}>Details</Link></PrimaryButton>
                </div>
            </div>
        </div>
    );
}
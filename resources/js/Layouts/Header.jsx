import { useState } from 'react';
import ApplicationLogo from '@/Components/ApplicationLogo';
import Dropdown from '@/Components/Dropdown';
import NavLink from '@/Components/NavLink';
import ResponsiveNavLink from '@/Components/ResponsiveNavLink';
import { Link } from '@inertiajs/react';
import AccountImage from '../../imgs/account.svg';
import LogOutImage from '../../imgs/login-out.svg';

export default function Header ({ auth }) {
    const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);
    
    return (
        <>
        
            <nav className="bg-white dark:bg-gray-800">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="flex justify-between h-16">
                        <div className="flex">
                            <div className="shrink-0 flex items-center">
                                <Link href="/" aria-label="Aller à la page d'accueil">
                                    <ApplicationLogo className="block h-16 w-auto fill-current" alt='Logo du site web'/>
                                </Link>
                            </div>

                            <div className='hidden items-center space-x-8 sm:-my-px sm:ml-10 sm:flex'>
                                <NavLink href={route('events')}>
                                    Evénements
                                </NavLink>
                                <NavLink href={route('contacts')}>
                                    Contact
                                </NavLink>
                            </div>

                        </div>

                        <div className='hidden sm:flex sm:items-center sm:ml-6'>
                            
                            {auth && auth.user ? (
                                <>
                                <div className='ml-3 relative flex flex-row'>
                                    <Dropdown>
                                        <Dropdown.Trigger>
                                            <img src={AccountImage} alt="Account picture" loading="lazy" />
                                        </Dropdown.Trigger>
                                        <Dropdown.Content>
                                            <Dropdown.Link href={route('dashboard')} active={route().current('dashboard') === 'dashboard' ? 'true' : 'false'}>Tableau de bord</Dropdown.Link>
                                            <Dropdown.Link href={route('profile.edit')} active={route().current('profil.edit') === 'profil.edit' ? 'true' : 'false'}>Mon profil</Dropdown.Link>
                                                <Dropdown.Link href='admin'>Admin</Dropdown.Link>
                                        </Dropdown.Content>
                                    </Dropdown>
                                    <Dropdown>
                                        <Dropdown.Trigger>
                                            <img className='' src={LogOutImage} alt="Logout picture" loading="lazy"/>
                                        </Dropdown.Trigger>
                                        <Dropdown.Content>
                                            <Dropdown.Link href={route('logout')} method="post" as="button">Se deconnecter</Dropdown.Link>
                                        </Dropdown.Content>
                                    </Dropdown>
                                </div>
                                </>
                                ) : ( 
                                <>
                                <div className='ml-3 relative'>
                                    <Dropdown>
                                        <Dropdown.Trigger>
                                            <div className='hover:bg-indigo-300 hover:bg:rounded-full'>
                                                <img className='' src={AccountImage} alt="Account picture" loading="lazy"/>
                                            </div>
                                        </Dropdown.Trigger>

                                        <Dropdown.Content>
                                            <Dropdown.Link href={route('register')} active={route().current('register') === 'register' ? 'true' : 'false'}>S'inscrire</Dropdown.Link>
                                            <Dropdown.Link href={route('login')} active={route().current('login') === 'login' ? 'true' : 'false'}>Se connecter</Dropdown.Link>
                                        </Dropdown.Content>
                                    </Dropdown>
                                </div>
                                </>
                            )}
                        </div>

                        <div className="-mr-2 flex items-center sm:hidden">
                            <button
                                onClick={() => setShowingNavigationDropdown((previousState) => !previousState)}
                                className="inline-flex items-center justify-center p-2 rounded-md hover:bg-gray-100 dark:hover:bg-gray-900 focus:outline-none focus:bg-gray-100 dark:focus:bg-gray-900 transition duration-150 ease-in-out"
                            >
                                <svg className="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                    <path
                                        className={!showingNavigationDropdown ? 'inline-flex' : 'hidden'}
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="M4 6h16M4 12h16M4 18h16"
                                    />
                                    <path
                                        className={showingNavigationDropdown ? 'inline-flex' : 'hidden'}
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="M6 18L18 6M6 6l12 12"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div className={(showingNavigationDropdown ? 'block' : 'hidden') + ' sm:hidden pt-6'}>
                    {auth && auth.user ? (
                        <div className="px-4">
                            <div className="font-semibold text-base">{auth.user.name}</div>
                            <div className="font-semibold text-sm">{auth.user.email}</div>
                        </div>
                    ) : null}
                        <div className="mt-3 space-y-1">
                            <ResponsiveNavLink href={route('events')}>
                                Evénements
                            </ResponsiveNavLink>
                            <ResponsiveNavLink href={route('contacts')}>
                                Contact
                            </ResponsiveNavLink>
                        </div>
                        <div className="mt-3 space-y-1">
                            <ResponsiveNavLink href={route('profile.edit')}>Mon profil</ResponsiveNavLink>
                            <ResponsiveNavLink href={route('dashboard')} active={route().current('dashboard')}>
                                Tableau de bord
                            </ResponsiveNavLink>
                            <ResponsiveNavLink method="post" href={route('logout')} as="button">
                                Se déconnecter
                            </ResponsiveNavLink>
                        </div>
                </div>
            </nav>

        </>
    );
}
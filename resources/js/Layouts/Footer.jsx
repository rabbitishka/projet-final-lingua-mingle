import ApplicationLogo from '@/Components/ApplicationLogo';
import { Link } from '@inertiajs/react';
import FacebookImage from '../../imgs/facebook.svg';
import InstagramImage from '../../imgs/instagram.svg';
import TwitterImage from '../../imgs/twitter.svg';

export default function Footer ({ }) {

    return (
        <>
        <div className='flex flex-col md:flex-row justify-evenly'>
            <div className='flex flex-col place-content-center items-center py-4'>
                        <Link href='/politique-de-confidentialité'>Politique de confidentialité</Link>
                        <Link href="/cgu">CGU</Link>
                        <Link href='/mentions-legales'>Mentions légales</Link>
            </div>
            <div className='flex flex-col items-center py-4'>
                <div className='flex flex-row place-content-center justify-evenly'>
                    <Link href='https://twitter.com/' target='_blank' onClick={() => handleClick('Twitter')}>
                        <img src={TwitterImage} alt="Twitter picture"/>
                    </Link> 
                    <Link href='https://facebook.com/' target='_blank' onClick={() => handleClick('Facebook')}>
                        <img src={FacebookImage} alt="Facebook picture"/>
                    </Link> 
                    <Link href='https://instagram.com/' target='_blank' onClick={() => handleClick('Instagram')}>
                        <img src={InstagramImage} alt="Instagram picture"/>
                    </Link>     
                </div>
                <div className='flex flex-col items-center py-4'>
                <p>Copyright © 2023 LinguaMingle</p>
                <p>Tous droits réservés.</p>
                </div>
            </div>
            <div className='flex justify-center md:items-center pb-4'>
                <Link href="/" aria-label="Aller à la page d'accueil">
                    <ApplicationLogo className="block h-16 w-auto fill-current" alt='Logo du site web'/>
                </Link>
            </div>
        </div>
        </>
    );
}
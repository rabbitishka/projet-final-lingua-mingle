import { useState, useEffect } from 'react';
import axios from 'axios';

const useRandomUnsplashImage = () => {
  const [randomImage, setRandomImage] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getRandomImage = async () => {
      try {
        const response = await axios.get('https://api.unsplash.com/photos/random', {
          params: {
            client_id: 'MHF-_YKUPV500seJ9UGOhj9xsRGCf5QfolsV8FhQu5g',
          },
        });
        // const imageAuthorUsername = response.data.user.username;
        // const authorResponse = await axios.get(`https://api.unsplash.com/users/${imageAuthorUsername}`, {
        // params: {
        //     client_id: 'MHF-_YKUPV500seJ9UGOhj9xsRGCf5QfolsV8FhQu5g',
        // },
        // });

        // const authorInfo = authorResponse.data;

        // setRandomImage({
        //         imageUrl: response.data.urls.regular,
        //         authorInfo: authorInfo,
        //         });
        setRandomImage(response.data.urls.regular);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching random image from Unsplash:', error);
        setLoading(false);
      }
    };

    getRandomImage();
  }, []);

//   return { randomImage: randomImage, loading: loading };
    return { randomImage, loading };
};

export default useRandomUnsplashImage;

export default function SecondaryButton({ type = 'button', className = '', disabled, children, ...props }) {
    return (
        <button
            {...props}
            type={type}
            className={
                `inline-flex items-center px-10 py-3 bg-white dark:bg-white border border-indigo-500 dark:border-indigo-500 rounded-full font-normal text-lg text-black dark:text-black tracking-wide hover:bg-indigo-100 dark:hover:bg-indigo-100 active:bg-indigo-500 active:text-white transition ease-in-out duration-150 ${
                    disabled 
                    // && 'opacity-25'
                } ` + className
            }
            disabled={disabled}
        >
            {children}
        </button>
    );
}

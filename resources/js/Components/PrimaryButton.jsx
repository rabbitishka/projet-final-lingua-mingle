export default function PrimaryButton({ className = '', disabled, children, ...props }) {
    return (
        <button
            {...props}
            className={
                `inline-flex items-center px-10 py-3 bg-indigo-500 dark:bg-indigo-500 
                border border-transparent rounded-full font-normal text-lg text-white dark:text-white-800 
                tracking-wide hover:bg-indigo-100 dark:hover:bg-indigo-100 hover:text-black 
                hover:border-indigo-500 focus:bg-white-700 dark:focus:bg-white active:bg-white 
                active:text-black active:border-indigo-500 dark:active:border-indigo-500 
                dark:active:bg-white focus:outline-none transition ease-in-out duration-150 ${
                    disabled 
                } ` + className
            }
            disabled={disabled}
        >
            {children}
        </button>
    );
}

import ConversationImage from '../../imgs/conversation.svg';
import PrimaryButton from './PrimaryButton';
import NavLink from '@/Components/NavLink';

export default function SectionEvents() {
    return (
        <>
            <div className="flex flex-col md:flex-row justify-center p-4">
            <div className=''>
                <img src={ConversationImage} alt="Conversation of group of people picture" loading="lazy"/>
            </div>
            <div className='flex flex-col justify-center'>
                <h2 className='text-center text-[24px] md:text-[32px] py-5'>Tous les événements</h2>
                <p className='text-center text-[16px] py-5 md:px-14'>Ici, vous pouvez sélectionner un événement en définissant les paramètres de recherche comme vous le souhaitez : langue, heure, lieu ou simplement explorer tous les événements à venir.</p>
                <div className='flex justify-center md:justify-end py-9'>
                    <PrimaryButton><NavLink href={route('events')}>Je participe</NavLink></PrimaryButton>
                </div>
            </div>
        </div>
        </>
    );
}
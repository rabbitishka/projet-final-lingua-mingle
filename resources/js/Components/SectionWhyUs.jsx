import VerifiedImage from '../../imgs/verified.svg';
import SharingImage from '../../imgs/pizza_sharing.svg';
import OnlineDiscussionImage from '../../imgs/online_discussion.svg';

export default function SectionWhyUs() {
    return (
        <>
            <div className='flex flex-col justify-center'>
                <h2 className='text-center text-[24px] md:text-[32px] py-5'>Pourquoi nous ?</h2>
                <div className='flex flex-col md:flex-row items-center gap-9 py-9'>
                    <div>
                        <img src={VerifiedImage} alt="Virified profile picture" loading="lazy"/>
                        <p className='text-center text-[20px] md:text-[24px] py-5'>Profils vérifiés</p>
                    </div>
                    <div>
                        <img src={SharingImage} alt="Sharing pizza picture" loading="lazy"/>
                        <p className='text-center text-[20px] md:text-[24px] py-5'>Partage d'intérêts</p>
                    </div>
                    <div>
                        <img src={OnlineDiscussionImage} alt="Online discussion picture" loading="lazy"/>
                        <p className='text-center text-[20px] md:text-[24px] py-5'>Présentiel ou en ligne</p>
                    </div>
                </div>
            </div>
        </>
    );
}
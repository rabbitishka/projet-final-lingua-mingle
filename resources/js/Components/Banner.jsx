import React from 'react';
import WorldImage from '../../imgs/world.svg';
import PrimaryButton from './PrimaryButton';
import SecondaryButton from './SecondaryButton';
import NavLink from '@/Components/NavLink';

export default function Banner() {
    return (
        <>
        <div className="flex flex-col md:flex-row justify-center p-4">
            <div className='flex flex-col justify-center'>
                <h1 className='text-center text-[32px] md:text-[40px] py-5'>Bienvenue chez LinguaMingle</h1>
                <h2 className='text-center text-[20px] md:text-[24px] py-5'>Pratiquons les langues du monde tous ensemble !</h2>
                <div className='flex flex-col md:flex-row justify-around items-center gap-9 py-6'>
                    <SecondaryButton><NavLink href={route('events.create')}>Je propose</NavLink></SecondaryButton>
                    <PrimaryButton><NavLink href={route('events')}>Je participe</NavLink></PrimaryButton>
                </div>
            </div>
            <div className=''>
                <img src={WorldImage} alt="Globe picture" loading="lazy"/>
            </div>
        </div>
        </>
    );
}
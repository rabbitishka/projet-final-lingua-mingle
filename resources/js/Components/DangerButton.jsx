export default function DangerButton({ className = '', disabled, children, ...props }) {
    return (
        <button
            {...props}
            className={
                `inline-flex items-center px-3 py-2 bg-red-600 border border-transparent rounded-full font-normal text-sm text-white tracking-widest hover:bg-red-500 active:bg-red-700 focus:outline-none transition ease-in-out duration-150 ${
                    disabled
                } ` + className
            }
            disabled={disabled}
        >
            {children}
        </button>
    );
}
